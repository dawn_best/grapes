Investigate better persistent tree representations. Arena likely not the best.

Custom PartialEq, Serialize, Deserialize implementations – take into account domain-specific knowledge. PartialEq currently considers some equivalent structs to not be equal. Deserialize can deserialize invalid trees.

Benchmark, test, optimize

