//! Mutable references to edges

use crate::graph::{Edge, EdgeId, Graph, NodeMut, NodeRef};

/// Mutable reference to an edge in a [Graph]
///
/// The library guarantees that this references a valid edge
#[derive(Debug)]
pub struct EdgeMut<'a, NodeData: Clone, EdgeData: Clone> {
    graph: &'a mut Graph<NodeData, EdgeData>,
    index: EdgeId,
}

impl<'a, NodeData: Clone, EdgeData: Clone> EdgeMut<'a, NodeData, EdgeData> {
    #[must_use]
    pub(crate) fn new(graph: &'a mut Graph<NodeData, EdgeData>, index: EdgeId) -> Self {
        Self { graph, index }
    }

    /// Get the Edge this [EdgeMut] is pointing to
    ///
    /// (internal convenience method)
    #[must_use]
    fn edge(&self) -> &Edge<EdgeData> {
        self.graph
            .edges
            .get(self.index.0)
            .expect("invariant violation: EdgeMut points to invalid edge")
    }

    /// Get a mutable reference to the Edge this [EdgeMut] is pointing to
    ///
    /// (internal convenience method)
    #[must_use]
    fn edge_mut(&mut self) -> &mut Edge<EdgeData> {
        self.graph
            .edges
            .get_mut(self.index.0)
            .expect("invariant violation: EdgeMut points to invalid edge")
    }

    /// The data associated with this edge
    ///
    /// (aka the edge weight)
    #[must_use]
    pub fn data(&self) -> &EdgeData {
        &self.edge().data
    }

    /// A mutable reference to data associated with this edge
    ///
    /// (aka the edge weight)
    #[must_use]
    pub fn data_mut(&mut self) -> &mut EdgeData {
        &mut self.edge_mut().data
    }

    /// The edge's identifier
    ///
    /// You can use this to later reference the edge in the graph (if it still exists)
    #[must_use]
    pub fn id(&self) -> EdgeId {
        self.index
    }

    /// Reference to the Node from which the edge starts
    ///
    /// (aka the tail of the arc)
    #[must_use]
    pub fn get_from_node(&self) -> NodeRef<NodeData, EdgeData> {
        NodeRef::new(self.graph, self.edge().from)
    }

    /// Mutable reference to the Node from which the edge starts
    ///
    /// (aka the tail of the arc)
    #[must_use]
    pub fn get_from_node_mut(&mut self) -> NodeMut<NodeData, EdgeData> {
        NodeMut::new(self.graph, self.edge().from)
    }

    /// Reference to the Node the edge points to
    ///
    /// (aka the head of the arc)
    #[must_use]
    pub fn get_to_node(&self) -> NodeRef<NodeData, EdgeData> {
        NodeRef::new(self.graph, self.edge().to)
    }

    /// Mutable reference to the Node the edge points to
    ///
    /// (aka the head of the arc)
    #[must_use]
    pub fn get_to_node_mut(&mut self) -> NodeMut<NodeData, EdgeData> {
        NodeMut::new(self.graph, self.edge().to)
    }

    /// Remove this edge from the graph
    ///
    /// Returns the data of the removed edge
    ///
    /// Note: there may be other edges with the same source and target nodes
    #[allow(clippy::must_use_candidate)]
    pub fn remove(self) -> EdgeData {
        let removed = self
            .graph
            .edges
            .remove(self.index.0)
            .expect("invariant violation: EdgeMut points to invalid edge");

        self.graph.detach_edge_from_from_list(&removed);
        self.graph.detach_edge_from_to_list(&removed);

        removed.data
    }
}
