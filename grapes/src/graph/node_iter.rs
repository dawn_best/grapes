//! Iterators over nodes

use crate::arena::ArenaItems;
use crate::graph::{Graph, Node, NodeId, NodeRef};

/// Iterator over all nodes in a Graph
///
/// Order is unspecified
#[must_use]
pub struct GraphNodes<'a, NodeData: Clone, EdgeData: Clone> {
    graph: &'a Graph<NodeData, EdgeData>,
    inner: ArenaItems<'a, Node<NodeData>>,
}

impl<'a, NodeData: Clone, EdgeData: Clone> GraphNodes<'a, NodeData, EdgeData> {
    pub(crate) fn new(graph: &'a Graph<NodeData, EdgeData>) -> Self {
        Self {
            graph,
            inner: graph.nodes.iter_items(),
        }
    }
}

impl<'a, NodeData: Clone, EdgeData: Clone> Iterator for GraphNodes<'a, NodeData, EdgeData> {
    type Item = NodeRef<'a, NodeData, EdgeData>;

    fn next(&mut self) -> Option<Self::Item> {
        let (id, _node) = self.inner.next()?;
        Some(NodeRef::new(self.graph, NodeId(id)))
    }
}
