use crate::graph;
use crate::graph::{EdgeId, NodeId};
use crate::map_graph::{EdgeEntry, EdgesFrom, EdgesTo, NodeEntry};
use imbl::HashMap;
use std::hash::Hash;

/// Mutable reference to a node in a [MapGraph](super::MapGraph)
///
/// The library guarantees that this references a valid node
#[derive(Debug)]
pub struct NodeMut<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::NodeMut<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    node_map: &'a mut HashMap<NodeKey, NodeId>,
    edge_map: &'a mut HashMap<EdgeKey, EdgeId>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > NodeMut<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub(crate) fn new(
        inner: graph::NodeMut<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
        node_map: &'a mut HashMap<NodeKey, NodeId>,
        edge_map: &'a mut HashMap<EdgeKey, EdgeId>,
    ) -> Self {
        Self {
            inner,
            node_map,
            edge_map,
        }
    }

    /// The data associated with the node
    ///
    /// (a.k.a. the node weight)
    #[must_use]
    pub fn data(&self) -> &NodeData {
        &self.inner.data().data
    }

    /// A mutable reference to the data associated with the node
    ///
    /// (a.k.a. the node weight)
    #[must_use]
    pub fn data_mut(&mut self) -> &mut NodeData {
        &mut self.inner.data_mut().data
    }

    /// The node's identifier
    ///
    /// You can use this to later reference the node in the graph (if it still exists)
    #[must_use]
    pub fn key(&self) -> &NodeKey {
        &self.inner.data().key
    }

    /// Iterate over edges leaving this node
    ///
    /// Order is unspecified.
    pub fn iter_edges_from(&self) -> EdgesFrom<NodeKey, NodeData, EdgeKey, EdgeData> {
        EdgesFrom::new(self.inner.iter_edges_from())
    }

    /// Iterate over edges entering this node
    ///
    /// Order is unspecified.
    pub fn iter_edges_to(&self) -> EdgesTo<NodeKey, NodeData, EdgeKey, EdgeData> {
        EdgesTo::new(self.inner.iter_edges_to())
    }

    /// Remove this node from the graph, along with all edges adjacent to it
    ///
    /// Returns the associated data of the removed node
    #[allow(clippy::must_use_candidate)]
    pub fn remove(self) -> NodeEntry<NodeKey, NodeData> {
        let entry = self
            .inner
            .graph
            .nodes
            .remove(self.inner.index.0)
            .expect("invariant violation: NodeMut points to invalid node");

        self.inner
            .graph
            .remove_all_edges_connecting(self.inner.index, &entry);

        self.node_map.remove(&entry.data.key);
        self.edge_map
            .retain(|_edge_key, edge_id| self.inner.graph.edges.get(edge_id.0).is_some());

        entry.data
    }
}
