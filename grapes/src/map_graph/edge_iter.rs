// Wrappers around graph::edge_iter iterators
//
// They produce graph_map::EdgeRef rather than graph::EdgeRef
// Very repetitive. I guess it could be done with macros, but that would harm readability.

use crate::graph;
use crate::map_graph::{EdgeEntry, EdgeRef, NodeEntry};
use std::hash::Hash;

/// Iterator over all edges leaving a node
#[must_use]
#[derive(Debug)]
pub struct EdgesFrom<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::EdgesFrom<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > EdgesFrom<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub(crate) fn new(
        inner: graph::EdgesFrom<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    ) -> Self {
        Self { inner }
    }
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > Iterator for EdgesFrom<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    type Item = EdgeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData>;

    fn next(&mut self) -> Option<Self::Item> {
        let inner = self.inner.next()?;
        Some(EdgeRef::new(inner))
    }
}

/// Iterator over all edges entering a node
#[must_use]
#[derive(Debug)]
pub struct EdgesTo<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::EdgesTo<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > EdgesTo<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub(crate) fn new(
        inner: graph::EdgesTo<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    ) -> Self {
        Self { inner }
    }
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > Iterator for EdgesTo<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    type Item = EdgeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData>;

    fn next(&mut self) -> Option<Self::Item> {
        let inner = self.inner.next()?;
        Some(EdgeRef::new(inner))
    }
}

/// Iterator over all edges in a graph
#[must_use]
pub struct GraphEdges<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::GraphEdges<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > GraphEdges<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub(crate) fn new(
        inner: graph::GraphEdges<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    ) -> Self {
        Self { inner }
    }
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > Iterator for GraphEdges<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    type Item = EdgeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData>;

    fn next(&mut self) -> Option<Self::Item> {
        let inner = self.inner.next()?;
        Some(EdgeRef::new(inner))
    }
}
