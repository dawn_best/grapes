use crate::map_tree;
use crate::tree::NodeId;

/// Describes a location in a tree
#[derive(Copy, Clone, Debug)]
pub enum Location {
    /// The next sibling of the specified node
    ///
    /// Inserting a node here would make that node the next sibling after the specified node, and push any further children down
    ///
    /// Note: the specified node should not be the root node, as the root may have no siblings
    AfterSibling(NodeId),
    /// The first child of the specified node
    ///
    /// Inserting a node here would make that node the first child, and push any other children down
    FirstChildOf(NodeId),
}

impl Location {
    /// Map the inner ID type of the Location
    pub fn into_mapped<NeyKey>(
        self,
        mapper: impl FnOnce(NodeId) -> NeyKey,
    ) -> map_tree::Location<NeyKey> {
        match self {
            Location::AfterSibling(id) => map_tree::Location::AfterSibling((mapper)(id)),
            Location::FirstChildOf(id) => map_tree::Location::FirstChildOf((mapper)(id)),
        }
    }
}
