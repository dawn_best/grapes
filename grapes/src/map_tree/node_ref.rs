use crate::map_tree::{Children, Entry};
use crate::tree;
use std::hash::Hash;

/// A reference to a node in a [MapTree](super::MapTree)
///
/// The library guarantees that this references a valid node in the associated tree
#[derive(Clone, Copy, Debug)]
pub struct NodeRef<'a, Key: Hash + Eq + Clone, T: Clone> {
    inner: tree::NodeRef<'a, Entry<Key, T>>,
}

impl<'a, Key: Hash + Eq + Clone, T: Clone> NodeRef<'a, Key, T> {
    #[must_use]
    pub(crate) fn new(inner: tree::NodeRef<'a, Entry<Key, T>>) -> Self {
        Self { inner }
    }

    /// Get the data associated with the node
    #[must_use]
    pub fn data(&self) -> &'a T {
        self.inner.data().value()
    }

    /// Get the key associated with the node.
    #[must_use]
    pub fn key(&self) -> &Key {
        self.inner.data().key()
    }

    /// Iterate over the node's children entries
    pub fn children(&self) -> Children<'a, Key, T> {
        Children::new(self.inner.children())
    }
}
