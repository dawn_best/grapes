Utility for "fuzz-testing" your tree implementation.

It applies random operations (insert, remove) to your tree and compares the output to a reference implementation – it uses [`id_tree`](https://crates.io/crates/id_tree) for this.

Note: this code is not very generic – I wrote it for my own very specific tree:
- Nodes indexable by an ID
- Children are ordered
- Tree always has root
- Tree locations are described relative to a parent or sibling

You might need to adapt it if you want to use it for a different tree.