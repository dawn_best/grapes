use std::fmt::Debug;
use std::hash::Hash;

use crate::Location;

pub trait TreeManipulation<Value> {
    type Key: Hash + Eq + Clone + Debug;

    fn new_with_root(value: Value) -> (Self::Key, Self);

    fn insert_node(&mut self, value: Value, location: Location<Self::Key>) -> Self::Key;

    fn move_node(&mut self, id: &Self::Key, new_location: Location<Self::Key>);

    fn remove(&mut self, id: &Self::Key);
}

pub trait TreeTraversal<Value> {
    type NodeId: Debug + Clone;

    fn root(&self) -> Self::NodeId;

    fn node<'a>(
        &'a self,
        node: Self::NodeId,
    ) -> (&'a Value, Box<dyn Iterator<Item = Self::NodeId> + 'a>);
}
