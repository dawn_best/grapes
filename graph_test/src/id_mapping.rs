use std::hash::Hash;

use bimap::BiHashMap;
use id_tree::NodeId;

use crate::Location;

pub struct IdMapping<T: Eq + Hash> {
    map: BiHashMap<NodeId, T>,
}

#[derive(Debug)]
pub struct MappingMismatch<T>(NodeId, T);

impl<T: Eq + Hash + Clone> Default for IdMapping<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: Eq + Hash + Clone> IdMapping<T> {
    pub fn new() -> Self {
        Self {
            map: BiHashMap::new(),
        }
    }

    pub fn register(&mut self, a: &NodeId, b: T) -> Result<(), MappingMismatch<T>> {
        if let Some(to) = self.map.get_by_left(a) {
            return if to == &b {
                Ok(())
            } else {
                Err(MappingMismatch(a.clone(), b))
            };
        }

        if let Some(from) = self.map.get_by_right(&b) {
            if from == a {
                unreachable!()
            } else {
                return Err(MappingMismatch(a.clone(), b));
            }
        }

        self.map.insert(a.clone(), b);

        Ok(())
    }

    pub fn lookup(&self, id: &NodeId) -> Option<&T> {
        self.map.get_by_left(id)
    }

    pub fn map_location(&self, location: Location<NodeId>) -> Location<T> {
        location.map(|id| self.lookup(id).expect("no such id").clone())
    }

    pub fn retain(&mut self, f: impl FnMut(&NodeId, &T) -> bool) {
        self.map.retain(f)
    }
}
