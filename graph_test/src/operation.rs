#[derive(Clone, Debug)]
pub enum Location<Key: Clone> {
    FirstChildOf(Key),
    NextSiblingOf(Key),
}

impl<Key: Clone> Location<Key> {
    pub fn map<NewKey: Clone>(&self, mapper: impl FnOnce(&Key) -> NewKey) -> Location<NewKey> {
        match &self {
            Location::FirstChildOf(k) => Location::FirstChildOf(mapper(k)),
            Location::NextSiblingOf(k) => Location::NextSiblingOf(mapper(k)),
        }
    }
}

#[derive(Clone, Debug)]
pub enum Operation<Key: Clone, Value> {
    Insert {
        value: Value,
        location: Location<Key>,
        reference_id: Key,
    },
    Move(Key, Location<Key>),
    Remove(Key),
}

pub struct CreateNew<Key, Value> {
    pub reference_id: Key,
    pub value: Value,
}
